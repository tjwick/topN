<?php
/**
 * PHP script to read a file line by line and report on highest N numbers
 *
 * Usage : $ php topN.php file n
 *   file: the file to read
 *   n: the top n numbers to report on
 *
 * Author: Tyler Wick
 * Date: May 2017
 */

/**
 * Function to sort an array numerically and exit if failure in sorting
 *
 * @param &$array array Address to array that we want to sort
 * @return none         Will exit if there is an error sorting
 */
function sortArrayNumeric(&$array)
{
	$sort = rsort($array, SORT_NUMERIC);

	if (!$sort)
	{
		echo "Error sorting array, exiting\n";
		exit(1);
	}
}

// Let's see if the args are set, else print a message and exit
if (!isset($argv[1]) || !isset($argv[2]))
{
	echo "Please pass in a file and an integer n, usage:\n";
	echo "php ".$argv[0]." file.txt 10\n";
	exit(1);
}

$fileName = $argv[1];
$topN = $argv[2];
$topArray = Array();

// Open file for reading
$fileHandle = fopen($fileName, "r");

// Could we not get the file open?
if (!$fileHandle)
{
	echo "Error trying to open file ".$fileName." for reading\n";
	exit(1);
}

while (($line = fgets($fileHandle)) !== FALSE)
{
	// Trim the line to get rid of any white space or line endings
	$trimmedLine = trim($line);
	$number = (int)$trimmedLine;

	// If the size is smaller than topN we know we must store this number and sort high to low
	if (count($topArray) <= $topN)
	{
		$topArray[] = $number;
		sortArrayNumeric($topArray);
	}
	else
	{
		// If the last element is less than our current number, remove it in place of the new number as we know this is sorted
		if ($topArray[$topN] < $number)
		{
			$topArray[$topN] = $number;
			sortArrayNumeric($topArray);
		}
	}
}

// Let's make sure we made it to the end of the file
if (!feof($fileHandle))
{
	echo "We didn't make it to the end of the file\n";
	exit(1);
}

echo "Here are the top ".$topN." numbers from file ".$fileName.":\n";
print_r($topArray);

?>
